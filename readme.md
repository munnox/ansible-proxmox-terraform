# Ansible to Terraform

Small project to develop better terraform systems for interaction with provisioning systems.

Uses the `provisioner` group to create the servers defined in `proxmox_guests` on the `proxmox` node

please build the templates for the local variables:

* `terraform_proxmox_auth.yml.template` to `terraform_proxmox_auth.yml` and place current proxmox cred into the yaml.
* `group_vars/proxmox.yml.template` to `group_vars/proxmox.yml` and place the current proxmox host details within.


```bash
pipenv install

pipenv shell

# Provison the VM's
ansible-playbook proxmox_create_vm.yml
# Destory the VM's
ansible-playbook proxmox_destroy_vm.yml
# Test comes to provisioners and guests
ansible-playbook proxmox_ping.yml
```